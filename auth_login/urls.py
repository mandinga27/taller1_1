from django.contrib import admin
from django.urls import path, include
from auth_login import views

app_name = 'auth'


urlpatterns = [
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout')
    #path('base/', views.base, name='base'),

]