from django.shortcuts import render, redirect
from django.contrib import auth #authenticate, login, logout

# Create your views here.


def login(request):
    template_name = 'login.html'
    data = {}

    auth.logout(request)
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(
            username=username,
            password=password,
        )

        if user is not None:
            ##flujo autenticado
            if user.is_active:
                # user valido
                auth.login(request, user)
                return redirect('basket:coach')

            else:
                print("Error! el usuario no esta activo")
                #user not valid
        else:
            print("Error! el usuario no existe")
            #user not valid

    return render(request, template_name, data)

def logout(request):
    auth.logout(request)

    return redirect('auth:login')