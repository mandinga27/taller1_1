from django.contrib import admin
from django.utils.safestring import mark_safe

# Register your models here.
from basket.models import (
    Coach,
    Team,
    Player,
    Roster,
    Match,
)

#usaremos decoradores para registrar nuestros mmodels
@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass

#estos son los campos que mostrara cuando entremos a Teams
@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    #['name', ] es porque hay una lista de equipos
    search_fields = ['name, ']
    list_display = (
        'name',
        'description',
        'code',
        'render_logo',
        'coach',
    )

    def render_logo(self, team):
        return mark_safe("<img src='/media/{}' width='80' height='80' />".format(team.logo))


#segun el ejercio Player debe permitir buscar por nombre
#apodo o rut
@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'team_code')
    list_filter = ('team', 'birthday')
    search_fields = ['name', 'nickname', 'rut']
    date_hierarchy = 'birthday'
    def team_code(self, player):
        return player.team.code
#creamos esta metodo custom para que el filtro muestre nombre del equipo y su codig


@admin.register(Roster)
class RosterAdmin(admin.ModelAdmin):
    pass

@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    pass