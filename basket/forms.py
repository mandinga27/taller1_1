from django import forms
from basket.models import Coach, Player, Team


class PlayersForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = '__all__'


class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = '__all__'


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = '__all__'


class ProfileForm(forms.Form):
    mail = forms.EmailField(label='Email', max_length=50)
    password = forms.CharField(max_length=20)

