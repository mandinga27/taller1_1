from django.db import models
from django.utils.safestring import mark_safe

# Create your models here.

"""
Coach
name
age
email
rut
nickname
"""
class Coach(models.Model):
	name = models.CharField(max_length=150)
	age = models.PositiveIntegerField()
	email = models.EmailField()
	rut = models.CharField(max_length=12)
	nickname = models.CharField(max_length=80)

	def __str__(self):
		return self.name


"""
Team
name
description
code
logo
"""
class Team(models.Model):
	name = models.CharField(max_length=150)
	description = models.TextField()
	# code tendra un valor unico -> unique=True
	code = models.CharField(max_length=20, unique=True)
	#logo es ImageFiel y se debe cargar en folder -> upload='teams/'
	logo = models.ImageField(upload_to='teams/')
	#coach tiene relacion 1:1
	coach = models.OneToOneField(Coach, on_delete=models.CASCADE)

	def __str__(self):
		return self.name

#para crear opciones desplegables se crean con tuplas y sub tuplas
POSITION_PLAYER = (
	('BA', 'Base'),
	('ES', 'Escolta'),
	('AL', 'Alero'),
	('AP', 'Ala-Pivot'),
	('PI', 'Pivot'),
)

class Player(models.Model):
	name = models.CharField(max_length=150)
	nickname = models.CharField(max_length=80)
	birthday = models.DateField()
	age = models.PositiveIntegerField()
	rut = models.CharField(max_length=12)
	email = models.EmailField()
	height = models.PositiveIntegerField(help_text='en centimetros')
	weight = models.PositiveIntegerField(help_text='in grams')
	photo = models.ImageField(upload_to='players/')
	#max_lenght=2 es porque en PISITION_PLAYER estan declarados dos strings -> BA, Base
	position = models.CharField(choices=POSITION_PLAYER, max_length=2)
	#declaramos la relacion 1:n referenciando a Player
	#la FK esta relacionando los jugadores con el equipo
	#3l primer valor seria el modelo al cual lo voy a relacionar
	team = models.ForeignKey(Team, on_delete=models.CASCADE)

	def __str__(self):
		return '{name} ({team})'.format(name=self.name, team=self.team)


#clase nomina Roster, mostrara los nombres de los jugadores nominados a un partido
#la relacionamons con la clase Palver y su cardinalidad es n:n
class Roster(models.Model):
	name = models.CharField(max_length=150)
	player = models.ManyToManyField(Player, help_text='Mantenga presionado "Control" o "Command" en Mac '
													  'para seleccionar más de un jugador, ')

	def __str__(self):
		return self.name


#crearemos la clase Match para armar los juegos que se agregaron en Roster(alineados de convocados)
#los Match ( partidos se gueran enrtre dos equipos -> Team1 y Team"
#los Match (partidos contienen una fecha del timpo DateTimeField)
#no se pueddn declarar dos ForeignKey -> se usa ManyToMany y solo lleva un argumento
class Match(models.Model):
	name = models.CharField(max_length=150)
	#teams = models.ManyToManyField(Roster)
	team1 = models.ForeignKey(Roster, related_name='team1', on_delete=models.CASCADE)
	team2 = models.ForeignKey(Roster, related_name='team2', on_delete=models.CASCADE)
	date = models.DateTimeField()

	def	__str__(self):
		return '{name} ({team2} vs {team2})'.format(
			name=self.name,
			team1=self.team1,
			team2=self.team2
		)









