from basket import views
from django.urls import path
#basket/create
#basket/players
#basket/

app_name = 'basket'

urlpatterns = [
    path('', views.home, name='home'),
    #path('login/', views.auth_login, name='auth'),
    path('base/', views.base, name='base'),
    path('players', views.players, name='players'),
    path('players/list', views.players_list, name='players_list'),
    path('coach/', views.coach, name='coach'),
    path('list_team/', views.list_team, name='teams'),
    path('match/', views.match, name='match'),
    #path('CoachList/', views.coach, name="coach"),
    path('create/player/', views.create_player, name="create_player"),
    path('update/player/<int:player_id>', views.update_player, name='update_player'),
    path('delete/player/<int:player_id>', views.delete_player, name='delete_player'),

    path('create/coach/', views.create_coach, name='create_coach'),
    path('update/coach/<int:coach_id>', views.update_coach, name="update_coach"),
    path('delete/coach/<int:coach_id>', views.delete_coach, name='delete_coach'),

    path('create/teams/', views.create_team, name='create_teams'),
    path('update/teams/<int:team_id>', views.update_teams, name='update_teams'),
    path('delete/team/<int:team_id>', views.delete_team, name='delete_team')

    #path('delete/<int:basket_id', views.delete, name='delete')

]

