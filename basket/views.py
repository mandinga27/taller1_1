from django.shortcuts import render, HttpResponse, redirect
from django.core.paginator import (Paginator,
                                   EmptyPage, 
                                   PageNotAnInteger)
from basket.models import Player, Coach, Team, Match
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import ListView, CreateView
from basket.forms import PlayersForm, TeamForm, CoachForm
# Create your views here.

@login_required
def home(request):
    data = {}
    template_name = 'home.html'
    #ata['title'] = 'Este es el Home'
    #ata['players'] = Player.objects.all().order_by('-team')[:5]
    #data.players.count()
    data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:3]
    data['last_5_players'] = Player.objects.all().order_by('-pk')[:3]
    data['teams'] = Team.objects.all()

    return render(request, template_name, data)

@login_required
def players_list(request):
    data = {}
    template_name = 'players_list.html'
    #data['players'] = Player.objects.all()

    teams = Team.objects.all()

    for team in teams:
        team.players = Player.objects.filter(team=team)

    data['teams'] = teams

    '''
    print('------------------')
    for team in teams:
        print(team.name)
        print('-------')
        for player in team.players:
            print(player.name)
        print('-------')
    '''
    return render(request, template_name, data)

@login_required
def players(request):
    data = {}
    template_name = 'players_list.html'
    #data['title'] = 'Players List'
    data['players'] = Player.objects.all()

    players = Player.objects.all()
    teams = Team.objects.all()

    for team in teams:
        teams.players = Player.objects.filter(team=team)

        data['teams'] = teams

    return render(request, template_name, data)


@login_required
def coach(request):

    print("PERMISOS")
    print(request.user.has_perm('basket.change_coach'))
    data = {}
    template_name = 'coach.html'

    print("User Login: ", request.user.pk)
    print("User Login", request.user.username)
    
    #data['title'] = 'Lista de entrenadores!!'
    data['coach'] = Coach.objects.all().order_by('-pk')[:3]


    return render(request, template_name, data)

def match(request):
    data = {}
    template_name = 'match.html'
    data['title'] = 'Lista de Matchs'
    data['match'] = Match.objects.all()

    return render(request, template_name, data)
"""
def poll_list(request):
    template_name = 'contacto.html'
    data = {}
    #traer todos los objetos de Player
    data['title'] = 'Lists Players from poll_list'
    #consulta con filtro juan
    data['jugadores'] = Player.objects.filter(
        name__contains='juan'
    )

    data['jugadores'] = Player.objects.all()

    return render(request, template_name, data)

"""


"""
def home(request):
    template_name = 'home.html'
    data = {}
    data['title'] = "Lista de jugadores desde home"
    data['players'] = Coach.objects.all()

    return render(request, template_name, data)
"""


#pagination in django
@login_required
def list_team(request):
    data = {}
    template_name = 'teams.html'

    data['title'] = "Lista de equipos indexados"

    page = request.GET.get('page', 1)
    #player_lists = Player.objects.all()
    team_lists = Team.objects.all()

    paginator = Paginator(team_lists, 3)

    try:
        data['teams'] = paginator.page(page)
    except PageNotAnInteger:
        data['teams'] = paginator.page(1)
    except EmptyPage:
        data['teams'] = paginator.page(paginator.num_pages)

    #paginator = Paginator(player_lists, 3)

    '''
    try:
        data['players'] = paginator.page(page)
    except PageNotAnInteger:
        data['players'] = paginator.page(1)
    except EmptyPage:
        data['players'] = paginator.page(paginator.num_pages)
    '''
    return render(request, template_name, data)


@login_required
def base(request):
    template_name = 'base.html'
    data={}
    data['title'] = "Desde la Base"

    return render(request, template_name)


@login_required
def create(request):
    data = {}
    template_name = 'create_coach.html'

    data['form'] = PlayersForm(request.POST or None)

    #aca se valida y se guarda el formulario
    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:players')

    return render(request, template_name, data)


@permission_required('basket.add_coach')
def create_coach(request):
    data = {}
    template_name = 'create_coach.html'

    data['form'] = CoachForm(request.POST or None)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:coach')

    return render(request, template_name, data)


@permission_required('basket.change_coach')
def update_coach(request, coach_id):
    template_name = 'update_coach.html'
    data = {}
    
    coach = Coach.objects.get(pk=coach_id)
    data['form'] = CoachForm(request.POST or None, instance=coach)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:coach')

    return render(request, template_name, data)


@permission_required('basket.delete_coach')
def delete_coach(request, coach_id):
    data = {}
    data['coach'] = Player.objects.get(pk=coach_id)

    if request.method == 'POST':
        data['coach'].delete()
        return redirect('basket:players')

    template_name = 'delete_coach.html'

    return render(request, template_name, data)


@login_required
def create_player(request):
    data = {}
    template_name = 'create_player.html'

    data['form'] = PlayersForm(request.POST or None, request.FILES)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:player_list')

    return render(request, template_name, data)


@login_required
def update_player(request, player_id):
    template_name = 'update_player.html'
    data = {}

    player = Player.objects.get(pk=player_id)
    data['form'] = PlayersForm(request.POST or None, instance=player)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:players')

    return render(request, template_name, data)


@login_required
def delete_player(request, player_id):
    data = {}
    data['player'] = Player.objects.get(pk=player_id)

    if request.method == 'POST':
        data['player'].delete()
        return redirect('basket:players')
        
    template_name = 'delete_player.html'

    return render(request, template_name, data)


@login_required
def create_team(request):
    data = {}
    template_name = 'create_teams.html'

    data['form'] = TeamForm(request.POST or None, request.FILES)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:teams')

    return render(request, template_name, data)


@login_required
def update_teams(request, team_id):
    template_name = 'update_teams.html'
    data = {}

    team = Team.objects.get(pk=team_id)
    data['form'] = TeamForm(request.POST or None, instance=team)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:teams_list')
    else:
        print("error al guardar")

    return render(request, template_name, data)


@login_required
def delete_team(request, team_id):
    data= {}
    data['team'] = Team.objects.get(pk=team_id)

    if request.method == 'POST':
        data['team'].delete()
        return redirect('basket:players')

    template_name = 'delete_team.html'

    return render(request, template_name, data)


#se agrega el coach profile
def profile_coach(request):
    template_name = 'profile_coach.html'
    data = {}

    return render(request, template_name, data)